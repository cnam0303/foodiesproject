/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import SplashScreen from './src/containers/SplashScreen'
import RootNavigator from './src/navigator/index'
import * as navigation from './src/utils/navigation'

const App: () => React$Node = () => {
  return (
    <>
        <SplashScreen/>
        <RootNavigator ref={nav => navigation.setNavigator(nav)}/>
    </>
  );
};


export default App;
