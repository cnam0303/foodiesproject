import {
    createAppContainer,
    createDrawerNavigator,
    createSwitchNavigator
  } from 'react-navigation';
  import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
  import { Transition } from 'react-native-reanimated';
  import React, { Component } from "react";

  import AuthScreen from '../containers/AuthScreen'
  import LoginScreen from '../containers/LoginScreen'
  import SignUpScreen from '../containers/SignUpScreen'

  
  const AuthStack = createAnimatedSwitchNavigator({
      AuthenScreen: AuthScreen,
      LoginScreen: LoginScreen,
      SignUpScreen: SignUpScreen
  }, {
     transition: (
        <Transition.Together>
          <Transition.In type="slide-right" durationMs={500} />
        </Transition.Together>
      ),
  })

  const RootNavigator = createSwitchNavigator({
      AuthStack: AuthStack
  })

  export default createAppContainer(RootNavigator)
